# Introduction

** Try it here: http://128.199.35.101:9999/num2word?n=125**

This is an exercise to convert numbers to words. For example, 3 => three, 124 => one hundred and twenty four.

The allowed numbers are between 1 and 1000. The function uses a recursive call to generate the pattern. We check the ranges of the
number passed in and perform the following:

```
0..9 => return word representing it
10..19 => return word represeting it (ten, eleven etc.)
20..99 => return MSB of the tens, and then call function with least significant bit (LSB). Return concat of both
100..999 => return MSB + 'hundred' + result of recursive call with digits excluding most significant bit (MSB)
1000 => return one thousand
```

# Structure

```
app/numeric.py # contains the function
app/main.py # contains flask app with single route
app/test.py # test suite
```



