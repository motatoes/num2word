import json
from flask import Flask
from flask import request
from numeric import num2word

app = Flask(__name__)

@app.route("/num2word", methods=["GET"])
def num2word_route():
    n = request.args.get("n", 0)
    try:
        n = int(n)
    except ValueError:
        return json.dumps(
            {"error": "invalid argument, expecting number"}
        )

    if n > 1000 or n < 0:
        return json.dumps(
            {"error": "invalid range: expecting range between 0 and 1000"}
        )
        
    result = num2word(n)
    return json.dumps({"data": result})

if __name__ == "__main__":
    # Only for debugging while developing
    app.run(host='0.0.0.0', debug=True, port=80)
